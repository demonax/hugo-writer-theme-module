// Mobile menu toggle
(function () {
  const menuButton = document.querySelector('#menu-button');
  const menuList = document.querySelector('#menu-list')
  const menuIcon = document.querySelector('.menu-icon');
  if (!menuButton) return;

  menuButton.addEventListener('click', function() {
    menuList.classList.toggle('show');
    menuIcon.classList.toggle('is-active');
  });
})();
