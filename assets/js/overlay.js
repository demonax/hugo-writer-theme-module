/* 
1. On page load, check for sessionStorage
2. If null, show .show-intro-popup class on html element
3. If set, remove .show-intro-popup on html element
4. On click of close button or maybe later button, remove .show-intro-popup
  */

// Home page modal
function checkPopup() {
  let hasPopup = document.documentElement.classList.contains('has-intro-popup');
  if(hasPopup) {
    // if sessionStorage has `showPopup` set, then do nothing. 
    // else set .show-intro-popup class on html element
    let openPop = sessionStorage.getItem('popupShown');
  
    if (!openPop) {
        // If not set, then set .show-intro-popup on overlay div
        document.documentElement.classList.add('show-intro-popup');
        sessionStorage.setItem('popupShown', true);
    }
  }
}

window.addEventListener('load', checkPopup);

function closePopup() {  
  document.documentElement.classList.remove('show-intro-popup');
}

var btns = document.querySelectorAll('.button');
for (const btn of btns) {
  btn.addEventListener('click', function () {   
      closePopup();
  });
}

  
/*

// Page header popup
// If this page needs to show the form, show it. 
function checkOptin() {
  let hasOptin = document.body.classList.contains('has-optin');

  if(hasOptin) {
    let openOptin = sessionStorage.getItem('optinShown');
  
    if (!openOptin) {      
      document.body.classList.add('show-optin');
      sessionStorage.setItem('optinShown', true);
    }
  }
}

window.addEventListener('load', checkOptin);

*/